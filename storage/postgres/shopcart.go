package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"market6405216/market_go_catalog_service/genproto/order_service"
	"market6405216/market_go_catalog_service/pkg/helper"
	"market6405216/market_go_catalog_service/storage"
)

type ShopcartRepo struct {
	db *pgxpool.Pool
}

func NewShopcartRepo(db *pgxpool.Pool) storage.ShopcartRepoI {
	return &ShopcartRepo{
		db: db,
	}
}

func (c *ShopcartRepo) Create(ctx context.Context, req *order_service.CreateShopcart) (resp *order_service.ShopcartPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "shopcarts" (
				shopcart_id,
		        order_id,
				product_id,
				count,
				size,
				type,
				total_summ,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.OrderId,
		req.ProductId,
		req.Count,
		req.Size,
		req.Type,
		req.TotalSumm,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.ShopcartPrimaryKey{ShopcartId: id.String()}, nil
}

func (c *ShopcartRepo) GetByPKey(ctx context.Context, req *order_service.ShopcartPrimaryKey) (resp *order_service.Shopcart, err error) {

	query := `
		SELECT
			shopcart_id,
			order_id,
			product_id,
			count,
			size,
			type,
			total_summ,
			created_at,
			updated_at
		FROM "shopcarts"
		WHERE shopcart_id = $1
	`

	var (
		shopcart_id sql.NullString
		order_id    sql.NullString
		product_id  sql.NullString
		count       sql.NullInt64
		size        sql.NullString
		typee       sql.NullString
		total_summ  sql.NullFloat64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetShopcartId()).Scan(
		&shopcart_id,
		&order_id,
		&product_id,
		&count,
		&size,
		&typee,
		&total_summ,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.Shopcart{
		ShopcartId: shopcart_id.String,
		OrderId:    order_id.String,
		ProductId:  product_id.String,
		Count:      count.Int64,
		Size:       size.String,
		Type:       typee.String,
		TotalSumm:  float32(total_summ.Float64),
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *ShopcartRepo) GetAll(ctx context.Context, req *order_service.GetListShopcartRequest) (resp *order_service.GetListShopcartResponse, err error) {

	resp = &order_service.GetListShopcartResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			shopcart_id,
			order_id,
			product_id,
			count,
			size,
			type,
			total_summ,g
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "shopcarts"

	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		filter = " AND barcode ILIKE " + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var shopcart order_service.Shopcart

		err := rows.Scan(
			&resp.Count,
			&shopcart.ShopcartId,
			&shopcart.OrderId,
			&shopcart.ProductId,
			&shopcart.Count,
			&shopcart.Size,
			&shopcart.Type,
			&shopcart.TotalSumm,
			&shopcart.CreatedAt,
			&shopcart.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Shopcarts = append(resp.Shopcarts, &shopcart)
	}

	return
}

func (c *ShopcartRepo) Update(ctx context.Context, req *order_service.UpdateShopcart) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "shopcarts"
			SET
				shopcart_id = $1,
				order_id = $2,
				product_id =$3,
				count =$4,
				size = $5,
				type = $6,
				total_summ = $7,
				updated_at = now()
			WHERE
				shopcart_id = $1`

	result, err := c.db.Exec(ctx, query,
		req.GetShopcartId(),
		req.GetOrderId(),
		req.GetProductId(),
		req.GetCount(),
		req.GetSize(),
		req.GetType(),
		req.GetTotalSumm(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ShopcartRepo) Delete(ctx context.Context, req *order_service.ShopcartPrimaryKey) error {

	query := `DELETE FROM "shopcarts" WHERE shopcart_id = $1`

	// query := `UPDATE "orders" SET deleted_at = now() WHERE order_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetShopcartId())

	if err != nil {
		return err
	}

	return nil
}
