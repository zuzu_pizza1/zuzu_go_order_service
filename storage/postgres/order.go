package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"market6405216/market_go_catalog_service/genproto/order_service"
	"market6405216/market_go_catalog_service/pkg/helper"
	"market6405216/market_go_catalog_service/storage"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) storage.OrderRepoI {
	return &OrderRepo{
		db: db,
	}
}

func (c *OrderRepo) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "orders" (
				order_id,
		        user_id,
				delivery_type,
				address,
				lat,
				long,
				home,
				floor,
				apartment,
				time_delivery,
				call_delivery,
				description,
				status,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.UserId,
		req.DeliveryType,
		req.Address,
		req.Lat,
		req.Long,
		req.Home,
		req.Floor,
		req.Apartment,
		req.TimeDelivery,
		req.CallDelivery,
		req.Description,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderPrimaryKey{OrderId: id.String()}, nil
}

func (c *OrderRepo) GetByPKey(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error) {

	query := `
		SELECT
			order_id,
			user_id,
			delivery_type,
			address,
			lat,
			long,
			home,
			floor,
			apartment,
			time_delivery,
			call_delivery,
			description,
			status,
			created_at,
			updated_at
		FROM "orders"
		WHERE order_id = $1
	`

	var (
		order_id      sql.NullString
		user_id       sql.NullString
		delivery_type sql.NullString
		address       sql.NullString
		lat           sql.NullString
		long          sql.NullString
		home          sql.NullString
		floor         sql.NullString
		apartment     sql.NullString
		time_delivery sql.NullString
		call_delivery sql.NullString
		description   sql.NullString
		status        sql.NullString
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetOrderId()).Scan(
		&order_id,
		&user_id,
		&delivery_type,
		&address,
		&lat,
		&long,
		&home,
		&floor,
		&apartment,
		&time_delivery,
		&call_delivery,
		&description,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.Order{
		OrderId:      order_id.String,
		UserId:       user_id.String,
		DeliveryType: delivery_type.String,
		Address:      address.String,
		Lat:          lat.String,
		Long:         long.String,
		Home:         home.String,
		Floor:        floor.String,
		Apartment:    apartment.String,
		TimeDelivery: time_delivery.String,
		CallDelivery: call_delivery.String,
		Description:  description.String,
		Status:       status.String,
		CreatedAt:    createdAt.String,
		UpdatedAt:    updatedAt.String,
	}

	return
}

func (c *OrderRepo) GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error) {

	resp = &order_service.GetListOrderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			order_id,
			user_id,
			delivery_type,
			address,
			lat,
			long,
			home,
			floor,
			apartment,
			time_delivery,
			call_delivery,
			description,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "orders"

	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		filter = " AND barcode ILIKE " + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var order order_service.Order

		err := rows.Scan(
			&resp.Count,
			&order.OrderId,
			&order.UserId,
			&order.DeliveryType,
			&order.Address,
			&order.Lat,
			&order.Long,
			&order.Home,
			&order.Floor,
			&order.Apartment,
			&order.TimeDelivery,
			&order.CallDelivery,
			&order.Description,
			&order.Status,
			&order.CreatedAt,
			&order.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Orders = append(resp.Orders, &order)
	}

	return
}

func (c *OrderRepo) Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "orders"
			SET
				order_id = $1,
				user_id = $2,
				delivery_type =$3,
				address =$4,
				lat = $5,
				long = $6,
				home = $7,
				floor = $8,
				apartment = $9,
				time_delivery = $10,
				call_delivery = $11,
				description = $12,
				status = $13,
				updated_at = now()
			WHERE
				order_id = $1`

	result, err := c.db.Exec(ctx, query,
		req.GetOrderId(),
		req.GetUserId(),
		req.GetDeliveryType(),
		req.GetAddress(),
		req.GetLat(),
		req.GetLong(),
		req.GetHome(),
		req.GetFloor(),
		req.GetApartment(),
		req.GetTimeDelivery(),
		req.GetCallDelivery(),
		req.GetDescription(),
		req.GetStatus(),
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *OrderRepo) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) error {

	query := `DELETE FROM "orders" WHERE order_id = $1`

	// query := `UPDATE "orders" SET deleted_at = now() WHERE order_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetOrderId())

	if err != nil {
		return err
	}

	return nil
}
