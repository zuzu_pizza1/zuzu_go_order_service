package storage

import (
	"context"
	"market6405216/market_go_catalog_service/genproto/order_service"
	// "market6405216/market_go_order_service/models"
)

type StorageI interface {
	CloseDB()
	Order() OrderRepoI
	Shopcart() ShopcartRepoI
}

type OrderRepoI interface {
	Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error)
	GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.OrderPrimaryKey) error
}

type ShopcartRepoI interface {
	Create(ctx context.Context, req *order_service.CreateShopcart) (resp *order_service.ShopcartPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.ShopcartPrimaryKey) (resp *order_service.Shopcart, err error)
	GetAll(ctx context.Context, req *order_service.GetListShopcartRequest) (resp *order_service.GetListShopcartResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateShopcart) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.ShopcartPrimaryKey) error
}
