package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"market6405216/market_go_catalog_service/config"
	"market6405216/market_go_catalog_service/genproto/order_service"
	"market6405216/market_go_catalog_service/grpc/client"
	"market6405216/market_go_catalog_service/grpc/service"
	"market6405216/market_go_catalog_service/pkg/logger"
	"market6405216/market_go_catalog_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	order_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))
	order_service.RegisterShopcartServiceServer(grpcServer, service.NewShopcartService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
