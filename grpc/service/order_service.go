package service

import (
	"context"
	"fmt"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"market6405216/market_go_catalog_service/config"
	"market6405216/market_go_catalog_service/genproto/order_service"
	"market6405216/market_go_catalog_service/genproto/user_service"

	"market6405216/market_go_catalog_service/grpc/client"
	"market6405216/market_go_catalog_service/pkg/logger"
	"market6405216/market_go_catalog_service/storage"
)

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderServiceServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderService) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.Order, err error) {

	i.log.Info("---CreateOrder------>", logger.Any("req", req))

	// user, err := i.services.UserService().GetById()

	pKey, err := i.strg.Order().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrder->Order->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Order().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyOrder->Order->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *OrderService) GetById(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error) {

	c.log.Info("---GetOrderByID------>", logger.Any("req", req))

	resp, err = c.strg.Order().GetByPKey(ctx, req)
	if err != nil {
		c.log.Error("!!!GetOrderByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	fmt.Println(c.services.UserService().GetById(ctx, &user_service.UserPrimaryKey{UserId: resp.UserId}))
	if err != nil {
		c.log.Error("!!!GetUserByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetList(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error) {

	i.log.Info("---GetOrders------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) Update(ctx context.Context, req *order_service.UpdateOrder) (resp *order_service.Order, err error) {

	i.log.Info("---UpdateOrder------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Order().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateOrder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Order().GetByPKey(ctx, &order_service.OrderPrimaryKey{OrderId: req.OrderId})
	if err != nil {
		i.log.Error("!!!GetOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *OrderService) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteOrder------>", logger.Any("req", req))

	err = i.strg.Order().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
