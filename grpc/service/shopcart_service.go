package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"market6405216/market_go_catalog_service/config"
	"market6405216/market_go_catalog_service/genproto/order_service"

	"market6405216/market_go_catalog_service/grpc/client"
	"market6405216/market_go_catalog_service/pkg/logger"
	"market6405216/market_go_catalog_service/storage"
)

type ShopcartService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedShopcartServiceServer
}

func NewShopcartService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ShopcartService {
	return &ShopcartService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ShopcartService) Create(ctx context.Context, req *order_service.CreateShopcart) (resp *order_service.Shopcart, err error) {

	i.log.Info("---CreateShopcart------>", logger.Any("req", req))

	// user, err := i.services.UserService().GetById()

	pKey, err := i.strg.Shopcart().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateShopcart->Shopcart->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Shopcart().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyShopcart->Shopcart->GetByID--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (c *ShopcartService) GetById(ctx context.Context, req *order_service.ShopcartPrimaryKey) (resp *order_service.Shopcart, err error) {

	c.log.Info("---GetShopcartByID------>", logger.Any("req", req))

	resp, err = c.strg.Shopcart().GetByPKey(ctx, req)
	if err != nil {
		c.log.Error("!!!GetShopcartByID->Shopcart->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopcartService) GetList(ctx context.Context, req *order_service.GetListShopcartRequest) (resp *order_service.GetListShopcartResponse, err error) {

	i.log.Info("---GetShopcarts------>", logger.Any("req", req))

	resp, err = i.strg.Shopcart().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShopcart->Shopcart->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopcartService) Update(ctx context.Context, req *order_service.UpdateShopcart) (resp *order_service.Shopcart, err error) {

	i.log.Info("---UpdateShopcart------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Shopcart().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateShopcart--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Shopcart().GetByPKey(ctx, &order_service.ShopcartPrimaryKey{ShopcartId: req.ShopcartId})
	if err != nil {
		i.log.Error("!!!GetShopcart->Shopcart->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShopcartService) Delete(ctx context.Context, req *order_service.ShopcartPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteShopcart------>", logger.Any("req", req))

	err = i.strg.Shopcart().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteShopcart->Shopcart->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
