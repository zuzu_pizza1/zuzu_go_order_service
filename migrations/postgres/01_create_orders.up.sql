
CREATE TABLE IF NOT EXISTS "orders" (
    "order_id" UUID PRIMARY KEY,
    "user_id" VARCHAR(50),
    "delivery_type" VARCHAR(50),
    "address" VARCHAR(50),
    "lat" VARCHAR(50) ,
    "long" VARCHAR(50),
    "home" VARCHAR(50),
    "floor" VARCHAR(50),
    "apartment" VARCHAR(50),
    "time_delivery" VARCHAR(50),
    "call_delivery" VARCHAR(50),
    "description" VARCHAR(50),
    "status" VARCHAR(50),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "shopcarts" (
    "shopcart_id" UUID PRIMARY KEY,
    "order_id" VARCHAR(50),
    "product_id" VARCHAR(50),
    "count" INTEGER   NOT NULL,
    "size" VARCHAR(50)   NOT NULL,
    "type" VARCHAR(50)   NOT NULL,
    "total_summ" FLOAT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);



SELECT
	COUNT(*) OVER(),
	product_id,
	barcode,
	product_name,
	COALESCE(price, 0),
	category_id,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "products"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 
